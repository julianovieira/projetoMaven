package servlets;

import dao.FuncionarioDAO;
import dto.FuncionarioDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/ListarFuncionarioServlet")
public class ListarFuncionarioServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
				
                out.println("<h1> Listagem de Funcionários </h1>");
		
                ArrayList<FuncionarioDTO> funcionarios = FuncionarioDAO.getFuncionarios();
		
		out.print("<table border='1' width='100%'");
		out.print("<tr><th>Id</th><th>Nome</th><th>Idade</th><th>telefone</th><th>Editar</th><th>Deletar</th></tr>");
                
		for(FuncionarioDTO a : funcionarios){                
			out.print("<tr><td>"+a.getId()+"</td><td>"+a.getNome()+"</td><td>"+a.getIdade()+"</td><td>"+a.getTelefone()+"</td><td><a href='EditServlet?id="+a.getId()+"'>editar</a></td><td><a href='DeleteServlet?id="+a.getId()+"'>deletar</a></td></tr>");
                        
		}
		out.print("</table>");
                
		out.close();
	}
}